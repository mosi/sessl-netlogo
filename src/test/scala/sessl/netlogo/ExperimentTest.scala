package sessl.netlogo

import java.net.URI

import org.junit.Assert._
import org.junit.Test
import sessl._

/**
  * @author Tom Warnke
  */
@Test class ExperimentTest {

  val testModel: URI = this.getClass.getResource("/Fire.nlogo").toURI

  @Test def runFire(): Unit = {
    execute {
      new Experiment with Observation with ParallelExecution with CSVOutput {
        model = testModel
        scan ("density" <~ range(60, 1, 70))
        replications = 5
        stopCondition = NetLogoExpression("ticks > 50") or AfterWallClockTime(seconds = 5)
        parallelThreads = -1

        observeAt(range(0, 5, 50))
        val b = observe("burned-trees")

        withRunResult(writeCSV)
      }
    }
  }
}
