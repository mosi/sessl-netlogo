package sessl.netlogo

import java.time.LocalDateTime
import java.time.temporal.ChronoUnit
import java.util.concurrent.{ExecutorService, Executors}

import org.nlogo.headless.HeadlessWorkspace
import sessl.{AbstractExperiment, AfterSimTime, AfterWallClockTime, ConjunctiveStoppingCondition,
  DisjunctiveStoppingCondition, DynamicSimulationRuns, StoppingCondition}

import scala.concurrent.{ExecutionContext, Future}

/**
  * @author Tom Warnke
  */
class Experiment extends AbstractExperiment with DynamicSimulationRuns {
  override type RunOutput = Unit

  override protected def basicConfiguration(): Unit = {}

  override protected[sessl] def executeExperiment(): Unit =
    startBlocking()

  override protected def startSimulationRun(runId: Int,
                                            assignment: Map[String, AnyRef]): Future[Unit] =
    Future {

      val workspace = HeadlessWorkspace.newInstance

      val startTime = now

      def durationInMs = ChronoUnit.MILLIS.between(startTime, now)

      def getStopCondition(stoppingCondition: StoppingCondition): Boolean =
        stoppingCondition match {
          case a: AfterSimTime => time(workspace) <= a.asSecondsOrUnitless
          case NetLogoExpression(exp) => test(workspace, exp)
          case a: AfterWallClockTime => durationInMs > a.asMilliSecondsOrUnitless
          case ConjunctiveStoppingCondition(l, r) =>
            getStopCondition(l) && getStopCondition(r)
          case DisjunctiveStoppingCondition(l, r) =>
            getStopCondition(l) || getStopCondition(r)
        }

      // open model file
      workspace.open(model)

      // set parameters
      for ((param, value) <- assignment)
        workspace.command(s"set $param $value")

      // run until stop time
      workspace.command("setup")
      processWorkspace(runId, workspace)

      while (!getStopCondition(checkAndGetStoppingCondition())) {
        workspace.command("go")
        processWorkspace(runId, workspace)
      }

      workspace.dispose()
    }

  protected def processWorkspace(runId: Int, workspace: HeadlessWorkspace): Unit = {}

  protected lazy val executor: ExecutorService = Executors.newSingleThreadExecutor()

  implicit private lazy val execContext: ExecutionContext =
    ExecutionContext.fromExecutor(executor)

  def time(workspace: HeadlessWorkspace): Double =
    workspace.report("ticks").toString.toDouble

  def test(workspace: HeadlessWorkspace, exp: String): Boolean =
    workspace.report(exp).toString.toBoolean

  private def now: LocalDateTime = java.time.LocalDateTime.now

  case class NetLogoExpression(exp: String) extends StoppingCondition

}
