package sessl.netlogo

import org.nlogo.headless.HeadlessWorkspace
import sessl.AbstractObservation.Observable
import sessl.util.SimpleObservation

/**
  * @author Tom Warnke
  */
trait Observation extends Experiment with SimpleObservation {

  private case class NetLogoObservable(varName: String) extends Observable[Int]

  implicit def stringToObservable(varName: String): Observable[Int] =
    getOrElseUpdate(varName, NetLogoObservable(varName))

  override protected def processWorkspace(runId: Int, workspace: HeadlessWorkspace): Unit = {
    super.processWorkspace(runId, workspace)

    val t = time(workspace)

    observables.collect {
      case obs@NetLogoObservable(varName: String) =>
        if (observationTimes.contains(t))
          addValueFor(runId, obs, (t, workspace.report(varName)))
    }
  }

}
